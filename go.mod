module github.com/trungtvq/latex

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/google/go-querystring v1.0.0
	github.com/magiconair/properties v1.8.1
	github.com/pelletier/go-toml v1.2.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.4.0
	github.com/trungtvq/go-utils v1.2.6
	go.uber.org/zap v1.12.0
)
