#!/bin/bash

HOME=$(pwd)
APPNAME=$(basename $HOME)
SERVER=3.0.1.198
SERVER_DIR=/zserver/go-projects/$APPNAME
USER=openvpnas
SERVER_ADDR=$USER@$SERVER:$SERVER_DIR

ssh -i "/Volumes/hdd/Drives/OneDrive/ssh/aws/trungtvq.pem" $USER@$SERVER sh $SERVER_DIR/runserver stop $SERVER_DIR/
rsync -e "ssh -i /Volumes/hdd/Drives/OneDrive/ssh/aws/trungtvq.pem" -avh --delete $HOME/$APPNAME $SERVER_ADDR/
rsync -e "ssh -i /Volumes/hdd/Drives/OneDrive/ssh/aws/trungtvq.pem" -avh --delete $HOME/*.toml $SERVER_ADDR/
rsync -e "ssh -i /Volumes/hdd/Drives/OneDrive/ssh/aws/trungtvq.pem" -avh --delete $HOME/runserver $SERVER_ADDR/

# runserver
ssh -i "/Volumes/hdd/Drives/OneDrive/ssh/aws/trungtvq.pem" $USER@$SERVER sh $SERVER_DIR/runserver start $SERVER_DIR
