package services

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/trungtvq/latex/monitor"
	"go.uber.org/zap"
	"os"
	"os/exec"
	"strings"
	"sync"
)

var (
	mux sync.Mutex
)

func Latex2Png(fileContent string) (string, error) {
	mux.Lock()
	defer mux.Unlock()
	//create file with content
	fileContent = strings.ReplaceAll(fileContent, "\t", "")
	err := createLatexFile(fileContent)
	if err != nil {
		return "", err
	}

	//convert file .tex to .png
	path := viper.GetString("storage.defaultLatexFilePath")
	name := viper.GetString("storage.defaultLatexFileName")
	out, err := convertLatexToPngWithDocker(path, viper.GetString("storage.defaultLatexFileName"))
	if err != nil {
		return "", err
	}
	monitor.Logger.Bg().Info("out docker:", zap.Any("cmd", out))
	//upload
	url, err := UploadToCloud(path, fmt.Sprintf("%s.png", name))
	if err != nil {
		return "", err
	}
	return url, nil
}

func createLatexFile(str string) error {
	f, err := os.Create(viper.GetString("storage.defaultLatexFilePath") + viper.GetString("storage.defaultLatexFileNameWithExtension"))
	if err != nil {
		return err
	}
	defer f.Close()

	strHead := `\documentclass[12pt]{article}

\usepackage{eqnarray,amsmath,amssymb}
\usepackage{mathrsfs}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{tikz,tkz-tab,tkz-linknodes}
\usepackage{tkz-euclide}
\usepackage{tikz-3dplot}
\usepackage[most]{tcolorbox}
\usepackage[tikz]{bclogo}
\usetkzobj{all}
\usetikzlibrary{arrows,calc,intersections,angles,quotes,shapes.geometric}
\usetikzlibrary{snakes}
\usepackage{tabvar}
\usepackage{venndiagram}
\usepackage{pgfplots}
\usepgfplotslibrary{fillbetween}
\usetikzlibrary{patterns}
%\pgfplotsset{compat=1.9}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=1.5cm] {geometry}
%\usepackage[unicode, bookmarks=false]{hyperref}
\usepackage[hidelinks,unicode]{hyperref}
\usepackage{icomma}
\usepackage{currfile}


%\usepackage[dethi]{ex_test} 
%\usepackage[loigiai]{ex_test}
%\usepackage[color]{ex_test}
%\def\colorEX{\color{blue}}
%\renewtheorem{ex}{\color{violet}Câu}
%\newtheorem{bt}{Bài}
\usepackage{esvect}
\usepackage[active,tightpage]{preview}
\PreviewBorder=12pt\relax
\begin{document}
\preview
{`

	strTail := `}
\endpreview
\end{document}`

	//write default head
	_, err = f.Write([]byte(strHead))
	if err != nil {
		return err
	}

	//write content
	_, err = f.Write([]byte(str))
	if err != nil {
		return err
	}

	//write default tail
	_, err = f.Write([]byte(strTail))
	if err != nil {
		return err
	}
	err = f.Sync()
	if err != nil {
		return err
	}
	return nil
}
func convertLatexToPngWithDocker(filePath, fileName string) ([]string, error) {

	cmdStr := fmt.Sprintf("docker run --rm -v %s:/var/workdir %s %s", filePath, viper.GetString("docker.image"), fileName)
	out, err := exec.Command("/bin/sh", "-c", cmdStr).Output()
	if err != nil {
		return nil, err
	}
	return strings.Split(string(out), "\n"), nil
}
