package services

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/trungtvq/latex/monitor"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	fmt.Printf("=========== Init Test ===========\n")
	viper.SetConfigFile("../config.test.toml")
	// Setting
	viper.AddConfigPath(".") // Path
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		panic(err.Error())
	}
	monitor.InitLoggerForTest()

	os.Exit(m.Run())
}
