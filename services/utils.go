package services

import (
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	"os"
	"reflect"
	"strings"
)

func RandomString(n int) string {
	var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	b := make([]rune, n)
	for i := range b {
		b[i] = letter[rand.Intn(len(letter))]
	}
	return string(b)
}

func SHA1Encrypt(params ...string) string {
	mixString := strings.Join(params, "|")
	sum := sha1.Sum([]byte(mixString))
	return hex.EncodeToString(sum[:])
}

//filePath not include fileName
func createMultipartReq(method, url, filePath, fileName string, extraFields map[string]string) (*http.Request, error) {
	b, w, err := createMultipartFormData("file", filePath, fileName, extraFields)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(method, url, &b)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	return req, nil
}
func createMultipartFormData(fileFieldName, filePath string, fileName string, extraFormFields map[string]string) (b bytes.Buffer, w *multipart.Writer, err error) {
	w = multipart.NewWriter(&b)
	var fw io.Writer
	file, err := os.Open(filePath + fileName)
	if err != nil {
		fmt.Println("err:", err)
		return
	}
	if fw, err = w.CreateFormFile(fileFieldName, fileName); err != nil {
		fmt.Println(err)
		return
	}
	if _, err = io.Copy(fw, file); err != nil {
		fmt.Println("err:", err)
		return
	}

	for k, v := range extraFormFields {
		w.WriteField(k, v)
	}

	w.Close()

	return
}

func mustOpen(f string) *os.File {
	r, err := os.Open(f)
	if err != nil {
		pwd, _ := os.Getwd()
		fmt.Println("PWD: ", pwd)
		panic(err)
	}
	return r
}

func parseBody(resp *http.Response, obj interface{}) error {
	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(obj)
}

// formatRequest generates ascii representation of a request
func dumpRequest(r *http.Request) {
	// Save a copy of this request for debugging.
	requestDump, err := httputil.DumpRequest(r, true)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("--------- REQUEST ---------")
	fmt.Println(string(requestDump))
	fmt.Println("--------- END-REQUEST ---------")

}

// dump response for testing
func dumpResponse(resp *http.Response) {
	dump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("--> [RESPONSE]:", string(dump))
}
func buildSignature(m map[string]string) string {
	return SHA1Encrypt(MapToReqParams(m) + "CS7b3Of4lxrKHFoLe8sVK9EyGIk")
}

func RemoveSliceFromTo(s []string, startRm, endRm int) []string {
	t := s
	s = s[:startRm]
	s = append(s, t[endRm:]...)
	return s
}

type TokenDraft struct {
	Name string
	Data []string
}

func MapToReqParams(m interface{}) string {
	result := ""
	val := reflect.ValueOf(m)
	if val.Kind() == reflect.Map {
		fmt.Println("len = ", val.Len())
		for _, element := range val.MapKeys() {
			if len(result) > 1 {
				result = fmt.Sprintf("%s&%s=%s", result, element, val.MapIndex(element))
			} else {
				result = fmt.Sprintf("%s=%s", element, val.MapIndex(element))
			}
		}
	}
	return result
}
