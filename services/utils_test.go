package services

import (
	"fmt"
	"github.com/spf13/viper"
	"regexp"
	"strings"
	"testing"
	"time"
)

func TestDDD(t *testing.T) {
	//c := createNewClient()
	m := map[string]string{
		"api_key":   "245182155387718",
		"timestamp": string(time.Now().Unix()),
	}
	buildSignature(m);
	//c.createMultipartReq("POST", "https://api.cloudinary.com/v1_1/trungtvq/image/upload", nil, )
}
func TestMapd(t *testing.T) {
	url, _ := UploadToCloud(viper.GetString("storage.defaultLatexFilePath"), "main.png")
	fmt.Println(url)
}
func TestRegex(t *testing.T) {
	str := `\begin{ex}%[Phan Minh Tâm, dự án EX11-2018]%[2-BGD-102 - Đề thi chính thức THPT QG môn Toán mã 102 năm 2018]%[2D2G5-5]
Cho phương trình $3^x+ m =\log _3(x - m)$ với $ m $ là tham số. Có bao nhiêu giá trị nguyên của $m \in ( -15; 15)$ để phương trình đã cho có nghiệm?
\choice
{$16$}
{$9$}
{\True $14$}
{$15$}
\loigiai{
	Ta có $ 3^x+m=\log_3(x-m)\Leftrightarrow3^x+x=\log_3(x-m)+x-m\ (*) $.\\
	Xét hàm số $ f(t)=3^t+t $ với $ t\in\mathbb{R} $, ta có $ f'(t)=3^t\ln 3+1>0,\forall t\in\mathbb{R} $ nên hàm số $ f(t) $ đồng biến trên tập xác định. Mặt khác, phương trình $ (*) $ có dạng $ f(x)=f(\log_3(x-m)) $. Do đó, $$ f(x)=f(\log_3(x-m))\Leftrightarrow x=\log_3(x-m)\Leftrightarrow 3^x=x-m\Leftrightarrow3^x-x=-m \ (**)$$
	Xét hàm số $ g(x)=3^x-x $ với $ x\in\mathbb{R} $, ta có $ g'(x)=3^x\ln3-1 $, $ g'(x)=0\Leftrightarrow x=\log_3\left( \dfrac{1}{\ln3} \right) = a $.\\
	Bảng biến thiên
	\begin{center}
			\begin{tikzpicture}[font=\footnotesize,line join= round,line cap=round,>=stealth,scale=0.85]
			\tkzTabInit[nocadre=false,lgt=1,espcl=2.5,deltacl=0.7pt]
			{$x$ /1,$g'(x)$ /1,$g(x)$ /2}
			{$-\infty$, $a$, $+\infty$}
			\tkzTabLine{ ,-,0,+, }
			\tkzTabVar{+/ $+\infty$, -/ $g(a)$, +/ $+\infty$}
			\end{tikzpicture}
	\end{center}
	Từ bảng biến thiên, ta suy ra phương trình $ (**) $ có nghiệm khi chỉ khi $ m\in\left(-\infty;-g\left(a \right)  \right)  $.\\
	Mặt khác, $ m \in \mathbb{Z} \ \cap \ (-15;15) $ nên $ m\in\lbrace -14;-13;-12;\ldots;-1  \rbrace $ (vì $ -g(a)\approx-0{,}9958452485 $).\\ Do đó, có $ 14 $ giá trị nguyên của $ m $ thỏa mãn yêu cầu bài toán.
}
\end{ex}`
	re := regexp.MustCompile(`\\begin\{([^}]*?)\}`)
	fmt.Println(re.FindAllStringSubmatch(str, 10))
	fmt.Println(re.FindAllSubmatchIndex([]byte(str), 1))

}
func TestReplaceWithRegex(t *testing.T) {
	str := "nani\thaha\nds"
	var re = regexp.MustCompile(`[^a-zA-Z0-9]*`)
	s := re.ReplaceAllString(str, ``)
	fmt.Println(s)
}
func TestSlice(t *testing.T) {
	s := []string{"1", "2", "3", "4", "5", "6"}

	fmt.Println("s:", RemoveSliceFromTo(s, 2, 6))
}
func TestRune(t *testing.T) {
	var startRune uint8
	startRune = '{'
	fmt.Println(string(rune(startRune)))
	str := "tai{sao}"
	fmt.Println(strings.Index(str, string(rune(startRune))))
}
func BenchmarkSlice(b *testing.B) {
	s := []string{"1", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6", "2", "3", "4", "5", "6"}
	for i := 0; i < b.N; i++ {
		RemoveSliceFromTo(s, 20, 30)
	}
}
func TestXinXo(t *testing.T) {
	str := ``
	var re = regexp.MustCompile(`(^|[^_])\bproducts\b([^_]|$)`)
	s := re.ReplaceAllString(str, `$1.$2`)
	fmt.Println(s)
}
