package services

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

func UploadToCloud(filePath, fileName string) (string, error) {
	m := map[string]string{
		"timestamp":     strconv.FormatInt(time.Now().Unix(), 10),
		"upload_preset": "latexpre",
	}
	m["api_key"] = "245182155387718"
	//m["signature"] = buildSignature(m)
	//uri := "https://api.cloudinary.com/v1_1/trungtvq/image/upload?" + MapToReqParams(m)
	uri := "https://api.cloudinary.com/v1_1/trungtvq/image/upload"
	if len(filePath) < 1 {
		filePath = "/Users/trung/Desktop/test/"
	}
	req, err := createMultipartReq("POST", uri, filePath, fileName, m)
	if err != nil {
		return "", err
	}
	c := &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 100,
		},
		Timeout: time.Duration(3000) * time.Second,
	}

	resp, err := c.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	j, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var data map[string]string
	json.Unmarshal(j, &data)
	return data["url"], nil
}
