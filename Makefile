REVISION = $(shell git log | head -n 1 | cut  -f 2 -d ' ')
APPNAME = zpi-gw-promotion
SERVER=10.30.83.2
SERVERDIR=/zserver/go-projects/zpi-gw-promotion
clean:
	rm -r $(EXEFILE)

build:
	go build -ldflags "-X main.revision=$(REVISION)"

buildlinux:
	GOOS=linux go build -ldflags "-X main.revision=$(REVISION)"

sync:
	sh rsync-dev.sh

deploy: buildlinux sync
