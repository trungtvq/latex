package monitor

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/trungtvq/go-utils/service/monitor/adapter/log"
)

var (
	//Logger log factory
	Logger     log.Factory
	HTTPLogger log.Factory
)

// InitLogger init log
func InitLogger() {
	logPath := viper.GetString("log.path")
	logLevel := viper.GetString("log.level")
	serviceName := viper.GetString("log.name")
	if logPath == "" || serviceName == "" {
		fmt.Println("Log service not working! Because it is not configured!")
		return
	}
	Logger = log.NewStandardFactory(logPath, serviceName, logLevel)
	HTTPLogger = log.NewStandardFactory(logPath, serviceName+"-http", logLevel)

	path := logPath + "/" + serviceName
	fmt.Println("Write log to", path, "- level", logLevel)
	fmt.Println("Write log to", path+"-http", "- level", logLevel)
}

//InitLoggerForTest .
func InitLoggerForTest() {
	Logger = log.NewStandardFactory("./test-log/", viper.GetString("service.name"), "debug")
	fmt.Println(fmt.Sprintf("Write log to ./test-log/%s-debug level", viper.GetString("service.name")))
}
