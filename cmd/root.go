package cmd

import (
	"github.com/trungtvq/latex/config"
	"github.com/trungtvq/latex/monitor"
	"os"

	"github.com/spf13/cobra"
)

var cfgFile string

func initService() {
	config.InitConfig(cfgFile)
	monitor.InitLogger()
}
func init() {
	cobra.OnInitialize(initService)
	RootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file as toml (default is config.dev.toml)")
}

// RootCmd Process message from Kafka, gRPC, RestAPI
var RootCmd = &cobra.Command{
	Use:   "Latex service",
	Short: "Many of function can be used in compile with shell script way",
}

// Execute ...
func Execute() {
	RootCmd.AddCommand(versionCmd)
	RootCmd.AddCommand(uploadCmd)
	if err := RootCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}
