package main

import "github.com/trungtvq/latex/cmd"

var revision string

func main() {
	cmd.SetRevision(revision)
	cmd.Execute()

}
