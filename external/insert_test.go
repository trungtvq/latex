package external

import (
	"testing"
)

func TestSendRequestWithOriginalToExternalToInsertToDatabase(t *testing.T) {
	query := GraphExternalBody{Query: `mutation createBigQuestion($bigQuestion: BigQuestionInput!) {createBigQuestion(bigQuestion: $bigQuestion) {_id}}`,
		Variables:
		Variables{BigQuestion: BigQuestion{
			Title:      "1233",
			Content:    "{}",
			Image:      "",
			Video:      "",
			Duration:   0,
			Categories: []string{"5da535e2e045012bcd9243ab"},
			SmallQuestionList: []SmallQuestion{
				SmallQuestion{
					QuestionType: 1,
					Answer:       "",
					Explanation:  `{\"blocks\":[{\"key\":\"cfp36\",\"text\":\"gt\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}`,
					Duration:     0,
					RandomChoice: false,
					Image:        "",
					Video:        "",
					File:         "",
					FileName:     "",
					AnswerFile:   "",
					MaximumWords: 0,
					MultiAnswer:  false,
					Order:        0,
					Question: DraftType{
						Blocks: [] DraftBlockType{DraftBlockType{
							Key:               "30j3m",
							Text:              "123",
							Type:              "unstyled",
							Depth:             0,
							InlineStyleRanges: nil,
							EntityRanges:      nil,
							Data:              "",
						},},
						EntityMap: nil,
					}.BuildToString(),
					Content:       "",
					HasScore:      true,
					ScoreByChoice: false,
					ChoiceList: []ChoiceType{
						ChoiceType{
							Content: DraftType{
								Blocks: []DraftBlockType{DraftBlockType{
									Key:               "bia2u",
									Text:              "1",
									Type:              "unstyled",
									Depth:             0,
									InlineStyleRanges: nil,
									EntityRanges:      nil,
									Data:              "",
								},},
								EntityMap: nil,
							}.BuildToString(),
							Image:     "",
							Audio:     "",
							IsActive:  true,
							IsAnswer:  false,
							IsLeft:    false,
							TrueOrder: 0,
							Order:     0,
						},
					},
				},
			},
		},
		},
	}
	InsertToDatabase(query, "token")
}
