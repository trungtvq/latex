package external

import (
	"encoding/json"
)

type GraphExternalBody struct {
	Query     string    `json:"query"`
	Variables Variables `json:"variables"`
}

type Variables struct {
	BigQuestion BigQuestion `json:"bigQuestion"`
}

type BigQuestion struct {
	Title   string `json:"title"`
	Content string `json:"content"`
	//Image             string          `json:"image"`
	//Video             string          `json:"video"`
	//Duration          int32           `json:"duration"`
	Categories        []string        `json:"categories"`
	SmallQuestionList []SmallQuestion `json:"smallQuestionList"`
}

type SmallQuestion struct {
	QuestionType int32  `json:"questionType"`
	Answer       string `json:"answer"`
	Explanation  string `json:"explanation"`
	//Duration      int32        `json:"duration"`
	RandomChoice bool `json:"randomChoice"`
	//Image         string       `json:"image"`
	//Video         string       `json:"video"`
	//File          string       `json:"file"`
	FileName string `json:"fileName"`
	//    string       `json:"answerFile"`
	MaximumWords  int32        `json:"maximumWords"`
	MultiAnswer   bool         `json:"multiAnswer"`
	Order         int32        `json:"order"`
	Question      string       `json:"question"` //DraftType
	Content       string       `json:"content"`
	HasScore      bool         `json:"hasScore"`
	ScoreByChoice bool         `json:"scoreByChoice"`
	ChoiceList    []ChoiceType `json:"choiceList"`
}

func BuildDraft(str string, uni int) DraftType {
	draft := DraftType{
		Blocks: []DraftBlockType{DraftBlockType{
			Key:               string(uni),
			Text:              "",
			Type:              "",
			Depth:             0,
			InlineStyleRanges: nil,
			EntityRanges:      nil,
		},},
		EntityMap: nil,
	}
	return draft
}
func (q SmallQuestion) BuildOneSmallQuestionList(question SimpleQuestion) SmallQuestion {
	q.Question = question.Question
	q.Answer = question.Answer
	q.Explanation = question.Explain
	for i, c := range question.Choice {
		jsonDraft, _ := json.Marshal(BuildDraft(c, i))
		choice := ChoiceType{
			Content:   string(jsonDraft),
			IsActive:  true,
			IsAnswer:  false,
			IsLeft:    false,
			TrueOrder: 0,
			Order:     0,
		}
		q.ChoiceList = append(q.ChoiceList, choice)
	}
	q.Content = "" //??
	q.HasScore = true
	q.QuestionType = 1
	return q
}
func BuildBigQuestion() {

}
func (s *SimpleQuestion) ParseTitle(title string) {
	s.Source = title
	s.Id = []string{"5da535e2e045012bcd9243ab"}
}

type DraftType struct {
	Blocks    []DraftBlockType         `json:"blocks"`
	EntityMap map[string]EntityMapType `json:"entityMap"`
}
type EntityMapType struct {
	Type       string      `json:"type"`
	Mutability string      `json:"mutability"`
	Data       interface{} `json:"data"`
}
type EntityMapData struct {
	TeX          string `json:"teX"`
	DisplayStyle bool   `json:"displaystyle"`
}
type EntityMapImage struct {
	Src string `json:"src"`
}
type EntityRange struct {
	Offset int32 `json:"offset"`
	Length int32 `json:"length"`
	Key    int32 `json:"key"`
}

func (d DraftType) BuildToString() string {
	j, err := json.Marshal(d)
	if err != nil {
		return ""

	}
	return string(j)
}

type DraftBlockType struct {
	Key               string            `json:"key"`
	Text              string            `json:"text"`
	Type              string            `json:"type"`
	Depth             int32             `json:"depth"`
	InlineStyleRanges []string          `json:"inlineStyleRanges"`
	EntityRanges      []EntityRange     `json:"entityRanges"`
	Data              map[string]string `json:"data"`
}

type ChoiceType struct {
	Content string `json:"content"` // DraftType
	//Image     string `json:"image"`
	//Audio     string `json:"audio"`
	//Answer string `json:"answer"`
	IsActive  bool  `json:"isActive"`
	IsAnswer  bool  `json:"isAnswer"`
	IsLeft    bool  `json:"isLeft"`
	TrueOrder int32 `json:"trueOrder"`
	Order     int32 `json:"order"`
}
type SimpleQuestion struct {
	Question string
	Source   string
	Id       []string
	Choice   []string
	Answer   string
	Explain  string
}

type Header struct {
	Value  string
	IsHide bool
}
