package bussiness

import (
	"errors"
	"fmt"
	"github.com/trungtvq/latex/external"
	"github.com/trungtvq/latex/monitor"
	"github.com/trungtvq/latex/services"
	"go.uber.org/zap"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func ParseMultiLineToDraftContent(strList []string) external.DraftType {
	//TODO: parse custom function
	urls, str := DetachCustomFunctionToImageLinks(strList)
	var count int
	strList = preprocessExpression(strings.Split(str, "\n"))
	var blockList []external.DraftBlockType
	mapList := make(map[string]external.EntityMapType)
	var mapListIndex int
	var currentKey int32
	for _, str := range strList {
		if len(strings.ReplaceAll(str, " ", "")) == 0 {
			continue
		}
		if strings.Contains(str, "####") {
			if urls != nil && count < len(urls) {
				entityRange := external.EntityRange{
					Offset: 0,
					Length: 1,
					Key:    currentKey,
				}
				currentKey++
				block := external.DraftBlockType{
					Key:               services.RandomString(10),
					Text:              " ",
					Type:              "atomic",
					Depth:             0,
					InlineStyleRanges: []string{},
					EntityRanges:      []external.EntityRange{entityRange},
					Data:              make(map[string]string),
				}
				blockList = append(blockList, block)

				entityMap := external.EntityMapType{
					Type:       "IMAGE",
					Mutability: "IMMUTABLE",
					Data: external.EntityMapImage{
						Src: urls[count],
					},
				}
				count++
				mapList[strconv.Itoa(mapListIndex)] = entityMap
				mapListIndex++
			}
		} else {
			block, entityMap := ParseOneLineToDraftContent(&currentKey, str)
			for _, e := range entityMap {
				mapList[strconv.Itoa(mapListIndex)] = e
				mapListIndex++
			}
			blockList = append(blockList, block)
		}

	}
	return external.DraftType{
		Blocks:    blockList,
		EntityMap: mapList,
	}
}

func DetachCustomFunctionToImageLinks(strList []string) (urls []string, normalStr string) {
	for i := 0; i < len(strList); i++ {
		var funcContent []string
		var isValid bool
		strList, funcContent, isValid = DetachStrFromStartToEnd(strList, &i)
		if isValid == false {
			monitor.Logger.Bg().Error("DetachCustomFunctionToImageLinks", zap.Any("strList", strList))
			return
		}
		if len(funcContent) > 0 {
			url, err := services.Latex2Png(strings.Join(funcContent, "\n"))
			if err != nil {
				urls = append(urls, strings.Replace(url, "http", "https", 1))
				continue
			}
			urls = append(urls, url)
		}

	}
	normalStr = strings.Join(strList, "\n")
	return
}

func DetachStrFromStartToEnd(strList []string, from *int) (newStrList []string, funcContent []string, isValid bool) {
	funcName := regexp.MustCompile(`\\begin\{([^}]*?)\}`)
	for ; *from < len(strList); *from++ {
		latexFunc := funcName.FindAllStringSubmatch(strList[*from], 1)

		//Found custom function
		if len(latexFunc) > 0 {
			funcContent = append(funcContent, strList[*from])
			strList[*from] = "####\n"
			*from++
			startDelete := *from
			for ; *from < len(strList); *from++ {
				if !strings.Contains(strList[*from], fmt.Sprintf("\\end{%s}", latexFunc[0][1])) {
					funcContent = append(funcContent, strList[*from])
				} else {
					funcContent = append(funcContent, strList[*from])
					isValid = true
					break
				}
			}

			//invalid format
			if isValid == false {
				return strList, nil, false
			}
			newStrList = services.RemoveSliceFromTo(strList, startDelete, *from+1)
			*from = startDelete - 1
			return
		}
	}

	return strList, nil, true
}
func preprocessExpression(strList []string) []string {
	r, _ := regexp.Compile(`[^\\]?\$`)

	var result []string
	for i := 0; i < len(strList); i++ {
		currentNumIndex := float64(len(r.FindAllStringIndex(strList[i], -1)))

		if math.Mod(currentNumIndex, 2) > 0.5 {
			//mapping to next line until EXPRESSION have VALID PAIR
			var nextLine string
			var validFlag bool
			for ; i+1 < len(strList); i++ {
				nextLine = strList[i] + strList[i+1]
				currentNumIndex = currentNumIndex + float64(len(r.FindAllStringIndex(strList[i+1], -1)))
				if math.Mod(currentNumIndex, 2) < 0.5 {
					validFlag = true
					i++
					break
				}
			}

			//map to result
			if validFlag == false {
				return []string{}
			} else {
				result = append(result, nextLine)
			}

		} else {
			result = append(result, strList[i])
		}
	}
	return result
}
func ParseOneLineToDraftContent(currentKey *int32, str string) (external.DraftBlockType, []external.EntityMapType) {

	var currentPos int32

	//list include expression and data
	l := strings.Split(str, "$")
	var entityMapList []external.EntityMapType
	entityRange := []external.EntityRange{}

	textInBlock := ""
	for i, s := range l {
		///////two case: expression & normal string
		s = strings.ReplaceAll(s, "\t", "")

		//expression case
		if math.Mod(float64(i), 2) > 0.5 {
			//textInBlock = textInBlock + " "
			//textInBlock = strings.Replace(textInBlock, `  `, ` `, 1)
			ex := strings.ReplaceAll(s, `\\`, `\`)
			ex = strings.ReplaceAll(ex, "\n", "")

			entityMapList = append(entityMapList, external.EntityMapType{
				Type:       "INLINETEX",
				Mutability: "IMMUTABLE",
				Data: external.EntityMapData{
					TeX:          ex,
					DisplayStyle: false,
				},
			})
			entityRange = append(entityRange, external.EntityRange{
				Offset: currentPos,
				Length: 2,
				Key:    *currentKey,
			})
			*currentKey++
			textInBlock = textInBlock + "\t\t"
			//increase position
			currentPos = currentPos + 2
		} else { //string (as simple) TODO: upgrade later
			ex := strings.ReplaceAll(s, "\r", ``)

			textInBlock = textInBlock + ex

			//increase position
			currentPos = currentPos + int32(len([]rune(ex)))

		}
	}
	return external.DraftBlockType{
		Key:               services.RandomString(10),
		Text:              textInBlock,
		Type:              "unstyled",
		Depth:             0,
		InlineStyleRanges: []string{},
		EntityRanges:      entityRange,
		Data:              make(map[string]string),
	}, entityMapList
}

func PreProcessStr(body string) (result string) {
	//add image code
	return ProduceComponentImg(body)
}

func ProduceComponentImg(component string) (newComponent string) {
	reg := regexp.MustCompile(`(\\immini|\\impicinpar)`)
	hasImg := reg.FindAllStringIndex(component, 1)
	if len(hasImg) > 0 {
		paragraphList := FindGraph(component[hasImg[0][0]:], 2)
		component = strings.Replace(component, fmt.Sprintf("{%s}", paragraphList[1]), "", 1)
		component = strings.Replace(component, `\choice`, fmt.Sprintf("%s\n\\choice", paragraphList[1]), 1)
		component = reg.ReplaceAllString(component, "")
	}
	return
}
func GetContentInside(str, start, end string) string {
	startPos := strings.Index(str, start)
	endPos := strings.Index(str, end)

	if endPos == -1 || startPos >= endPos {
		return str
	}
	return str[startPos:endPos]
}
func RemoveExcess(str, start, end string) string {
	startPos := strings.Index(str, start)
	endPos := strings.Index(str, end)

	if endPos == -1 || startPos >= endPos {
		return str
	}
	return fmt.Sprintf("%s%s%s", str[:startPos], str[startPos+len(start):endPos], str[endPos+len(end):])
}

//find first match, then copy string to end match, return postion of end
func DetachList(str string, start string, end string) ([]string, int32, error) {
	strResultStartPos := strings.Index(str, start)
	strResultEndPos := strings.Index(str, end)

	if strResultEndPos == -1 {
		return nil, 0, errors.New("end position is not exited")
	}
	return strings.Split(str[strResultStartPos+len(start):strResultEndPos], "\n"), int32(strResultEndPos + len(end)), nil

}

//DetachBodyList find all string satisfy lie between (start and end)
func DetachBodyList(str string, start string, end string) []string {
	if start == end {
		return nil
	}
	var result []string
	for ; ;
	{
		startIndex := strings.Index(str, start)
		endIndex := strings.Index(str, end)
		if startIndex != -1 && endIndex != -1 {
			if startIndex > endIndex {
				str = str[endIndex+len(end):]
				continue
			}
			result = append(result, str[startIndex+len(start):endIndex])
			str = str[endIndex+len(end):]
		} else {
			break
		}
	}
	return result
}

func DetachChoice(str string) ([]string, int32) {
	var count int32
	var result []string
	var tmp []rune
	var answer int32
	var in bool
	var index int32
	runes := []rune(str)

	for _, s := range runes {
		if s == '}' {
			if count == 1 {
				s := string(tmp)
				if strings.Contains(string(tmp), `\True`) {
					s = strings.Replace(s, `\True`, ``, 1)
					answer = index
				}
				result = append(result, s)
				index++
				tmp = tmp[0:0]
				count = 0
				in = false
				continue
			} else {
				count--
			}
		} else if s == '{' {
			count++
			if count == 1 {
				in = true
				continue
			}
		}
		if in == true {
			tmp = append(tmp, s)
		}
	}
	return result, answer
}

func FindGraph(str string, num int) (result []string) {
	var count int32
	var tmp []rune
	var in bool
	var index int32
	runes := []rune(str)

	for _, s := range runes {
		if s == '}' {
			if count == 1 {
				s := string(tmp)

				result = append(result, s)
				if len(result) >= num {
					return
				}
				index++
				tmp = tmp[0:0]
				count = 0
				in = false
				continue
			} else {
				count--
			}
		} else if s == '{' {
			count++
			if count == 1 {
				in = true
				continue
			}
		}
		if in == true {
			tmp = append(tmp, s)
		}
	}
	return
}

func parseHeader(header string) (external.Header, []string) {
	var IDs []string
	var source external.Header

	titleList := DetachBodyList(header, "[", "]")

	for _, t := range titleList {
		if len(t) > 7 {
			source.Value = t
		} else {
			IDs = append(IDs, t)
		}
	}
	IDs = []string{"5da535e2e045012bcd9243ab"}
	return source, IDs
}
