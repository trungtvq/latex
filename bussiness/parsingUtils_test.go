package bussiness

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"os/exec"
	"regexp"
	"testing"
)

func TestParseString(t *testing.T) {
	str := `Mặt phẳng $(P)\colon \ x+2y+3z-5=0$ có một véc-tơ pháp tuyến là $\overrightarrow{n}_2=(1;2;3)$.}`
	var g int32
	block, entityMap := ParseOneLineToDraftContent(&g, str)
	fmt.Println(block, entityMap)
}
func TestDetachBody(t *testing.T) {
	s := DetachBodyList(`\begin{ex}%[3THPT QUỐC GIA 2018 - 101]%[Nguyễn Thành Sơn, dự án 12EX11]%[2H3Y2-2]
	Có bao nhiêu cách chọn hai học sinh từ một nhóm gồm $34$ học sinh?
	\choice
	{$2^{34}$}
	{$\mathrm{A}_{34}^2$}
	{$34^2$}
	{\True $\mathrm{C}_{34}^2$}
	\loigiai{
		Mỗi cách chọn hai học sinh từ một nhóm gồm $34$ học sinh là một tổ hợp chập $2$ của $34$ phần tử nên số cách chọn là $\mathrm{C}_{34}^2$.}
\end{ex}
\begin{ex}%[1THPT QUỐC GIA 2018 - 101]%[Nguyễn Thành Sơn, dự án 12EX11]%[2H3Y2-2]
	Trong không gian $Oxyz$, mặt phẳng $(P)\colon \ x+2y+3z-5=0$ có một véc-tơ pháp tuyến là
	\choice
	{$\overrightarrow{n}_1=(3;2;1)$}
	{$\overrightarrow{n}_3=(-1;2;3)$}
	{$\overrightarrow{n}_4=(1;2;-3)$}
	{\True $\overrightarrow{n}_2=(1;2;3)$}
	\loigiai{
		Mặt phẳng $(P)\colon \ x+2y+3z-5=0$ có một véc-tơ pháp tuyến là $\overrightarrow{n}_2=(1;2;3)$.}
\end{ex}
\begin{ex}%[2THPT QUỐC GIA 2018 - 101]%[Nguyễn Thành Sơn, dự án 12EX11]%[2D1Y2-2]
	\immini{Cho hàm số $y=ax^3+bx^2+cx+d\ (a,\, b,\, c,\, d \in \mathbb{R})$ có đồ thị như hình vẽ bên. Số điểm cực trị của hàm số đã cho là
	\choice
	{\True $2$}
	{$0$}
	{$3$}
	{$1$}}
	{\begin{tikzpicture}[>=stealth,scale=.25]
		\clip(-4.5,-4) rectangle (5.2,6);
		\draw[->] (-4.5,0) -- (4.5,0) node[below]{$x$};
		\draw[->] (0,-5.5) -- (0,5.4) node[left]{$y$};
		\draw[smooth,samples=100,domain=-4.1:4] plot(\x,{1/4*(\x)^3-3*(\x)+1});
		\draw [fill=white,draw=black] (0,0) circle (1pt)node[below left]{\footnotesize $O$};
	\end{tikzpicture}}
	\loigiai{
		Dựa vào đồ thị ta khẳng định hàm số đã cho có $2$ điểm cực trị.}
\end{ex}`, `\begin`, `\end`)
	fmt.Println(len(s))
}
func TestDetach(t *testing.T) {
	s, i, _ := DetachList(`\begin nanidfnldf \end qon`, `\begin`, `\end`)
	fmt.Println(s, i)
}

func TestDetachChoice(t *testing.T) {
	a, ans := DetachChoice(`\choice
	{\True $2$}
	{$0$}
	{$3$}
	{$1$}}`)
	fmt.Println(a, ans)
	assert.Equal(t, len(a), 4)
}
func TestDd(t *testing.T) {
	cmd := exec.Command("cp", "draft.go", "xxx.go")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		//log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	if len(errStr) > 0 {
		fmt.Printf("err:\n%s\n", errStr)

	}
	fmt.Printf("out:\n%s\n", outStr)
}
func TestTt(t *testing.T) {
	var str []string
	str = append(str, "12345")
	str = append(str, "qwert")

	test(str)
	fmt.Println(str)
}
func test(str []string) string {
	str[1] = "111"
	return str[0]
}

func TestMakeCustomBody(t *testing.T) {
	newBody := `\immini{Đường cong trong hình vẽ bên là đồ thị của hàm số nào dưới đây?
		\choice
		{$y=-x^4+x^2-1$}
		{$y=x^4-3x^2-1$}
		{$y=-x^3-3x-1$}
		{\True $y=x^3-3x-1$}
	}
	{\begin{tikzpicture}[scale=0.7, font=\footnotesize, line join=round, line cap=round,>=stealth]
		\def\a{1} \def\b{0} \def\c{-3} \def\d{-1}
		\def\xmin{-2.5} \def\xmax{2.7}
		\def\ymin{-3.5} \def\ymax{2} 
		\draw[->] (\xmin,0)--(\xmax,0) node [below]{$x$};
		\draw[->] (0,\ymin)--(0,\ymax) node [right]{$y$};
		\node at (0,0) [below right]{$O$};
		\clip (\xmin+0.1,\ymin+0.1) rectangle (\xmax-0.5,\ymax-0.1);
		\draw[smooth,samples=300] plot(\x,{\a*(\x)^3+\b*(\x)^2+\c*(\x)+\d});
		\tkzDefPoints{0/0/O}
		\tkzDrawPoints[fill=black](O)
		\end{tikzpicture}}
	\loigiai{
		Đường cong trên hình là đồ thị của hàm số bậc ba với hệ số $a>0$ nên chỉ có $y=x^3-3x-1$ là đúng.}`
	reg := regexp.MustCompile(`(\\immini|\\impicinpar)`)
	hasImg := reg.FindAllStringIndex(newBody, 1)

	for i, r := range hasImg {
		fmt.Println(i, r)
	}
	fmt.Println("-------------")
	a, b := DetachChoice(newBody[hasImg[0][0]:])
	for _, p := range a {
		fmt.Println(len(a), p, b)

	}
}
func TestParseU(t *testing.T) {
	str := `\begin{ex}%[Đề 103, THPT.QG - 2018]%[Dương Phước Sang, 12-Ex11-2018]%[2D1Y2-2]
	\immini{Cho hàm số $y=ax^4+bx^2+c$ ($a$, $b$, $c\in \mathbb{R}$) có đồ thị như hình vẽ bên.
		Số điểm cực trị của hàm số đã cho là
		\choice
		{$2$}
		{\True $3$}
		{$0$}
		{$1$}}
	{\begin{tikzpicture}[scale=0.8, font=\footnotesize, line join=round, line cap=round,>=stealth]
		\def\a{1/5} \def\b{-4/5} \def\c{-1/2}
		\def\xmin{-3} \def\xmax{3}
		\def\ymin{-1.5} \def\ymax{1}  
		\draw[->] (\xmin,0)--(\xmax,0) node [above]{$x$};
		\draw[->] (0,\ymin)--(0,\ymax) node [left]{$y$};
		\node at (0,0) [above left]{$O$};
		\clip (\xmin+0.1,\ymin+0.1) rectangle (\xmax-0.5,\ymax-0.5);
		\draw[smooth,samples=300] plot(\x,{\a*(\x)^4+\b*(\x)^2+\c});
		\tkzDefPoints{0/0/O}
		\tkzDrawPoints[fill=black](O)
	\end{tikzpicture}}
	\loigiai{}
\end{ex}`
	fmt.Println(ProduceComponentImg(str))
}
func TestRemoveExcess(t *testing.T) {
	str := `\begin{ex}%[Đề 103, THPT.QG - 2018]%[Dương Phước Sang, 12-Ex11-2018]%[2D1Y2-2]
	\immini{Cho hàm số $y=ax^4+bx^2+c$ ($a$, $b$, $c\in \mathbb{R}$) có đồ thị như hình vẽ bên.
		Số điểm cực trị của hàm số đã cho là
		\choice
		{$2$}
		{\True $3$}
		{$0$}
		{$1$}}
	{\begin{tikzpicture}[scale=0.8, font=\footnotesize, line join=round, line cap=round,>=stealth]
		\def\a{1/5} \def\b{-4/5} \def\c{-1/2}
		\def\xmin{-3} \def\xmax{3}
		\def\ymin{-1.5} \def\ymax{1}  
		\draw[->] (\xmin,0)--(\xmax,0) node [above]{$x$};
		\draw[->] (0,\ymin)--(0,\ymax) node [left]{$y$};
		\node at (0,0) [above left]{$O$};
		\clip (\xmin+0.1,\ymin+0.1) rectangle (\xmax-0.5,\ymax-0.5);
		\draw[smooth,samples=300] plot(\x,{\a*(\x)^4+\b*(\x)^2+\c});
		\tkzDefPoints{0/0/O}
		\tkzDrawPoints[fill=black](O)
	\end{tikzpicture}}
	\loigiai{}
\end{ex}`
	RemoveExcess(str, "\\immini{", )
}
