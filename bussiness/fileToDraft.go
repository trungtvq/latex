package bussiness

import (
	"errors"
	"fmt"
	"github.com/trungtvq/latex/external"
	"regexp"
	"strings"
)

func FileToListQuestionBody(file []byte) ([]external.GraphExternalBody, error) {
	var questions []external.GraphExternalBody
	commonQuery := `mutation createBigQuestion($bigQuestion: BigQuestionInput!) {createBigQuestion(bigQuestion: $bigQuestion) {_id} } `
	str := string(file)
	bodyList := DetachBodyList(str, `begin{ex}`, `\end{ex}`)
	for _, body := range bodyList {
		var smallQuestion external.SmallQuestion
		smallQuestion.Content = "{}"
		var newBody string
		reg := regexp.MustCompile(`(\\immini|\\impicinpar)`)

		hasImg := reg.FindAllStringIndex(body, 1)
		if len(hasImg) > 0 {
			paragraphList := FindGraph(body[hasImg[0][0]:], 2)
			newBody = strings.Replace(body, fmt.Sprintf("{%s}", paragraphList[1]), "", 1)
			newBody = strings.Replace(newBody, `\choice`, fmt.Sprintf("%s\n\\choice", paragraphList[1]), 1)
			newBody = reg.ReplaceAllString(newBody, "")
		}
		//parse header
		header, pos, err := DetachList(newBody, "", "\n")
		if err != nil {
			return nil, err
		}
		if len(header) != 1 {
			return nil, errors.New("header is invalid")
		}

		//parse question
		newBody = newBody[pos:]
		question, pos, err := DetachList(newBody, ``, `\choice`)
		if err != nil {
			return nil, err
		}
		//TODO: add hotfix for unusual case

		smallQuestion.Question = ParseMultiLineToDraftContent(question).BuildToString()
		smallQuestion.QuestionType = 1
		//parse choice
		newBody = newBody[pos:]
		choice, pos, err := DetachList(newBody, ``, `\loigiai{`)
		var answer int32
		var strList []string
		if err == nil {
			strList, answer = DetachChoice(strings.Join(choice, "\n"))

			//then parse loigiai
			newBody = newBody[pos:]
			loigiai := strings.Split(newBody, "\n")

			//mapping
			smallQuestion.Explanation = ParseMultiLineToDraftContent(loigiai).BuildToString()

		} else {

			//then detach one by one choice
			strList, answer = DetachChoice(newBody)
			smallQuestion.Explanation = ParseMultiLineToDraftContent([]string{""}).BuildToString()

		}

		for i, c := range strList {
			if int32(i) == answer {
				smallQuestion.ChoiceList = append(smallQuestion.ChoiceList, external.ChoiceType{
					Content:   ParseMultiLineToDraftContent(strings.Split(c, "\n")).BuildToString(),
					IsActive:  true,
					IsAnswer:  true,
					IsLeft:    false,
					TrueOrder: 0,
					Order:     0,
				})
			} else {
				smallQuestion.ChoiceList = append(smallQuestion.ChoiceList, external.ChoiceType{
					Content: ParseMultiLineToDraftContent(strings.Split(c, "\n")).BuildToString(),

					IsActive:  true,
					IsAnswer:  false,
					IsLeft:    false,
					TrueOrder: 0,
					Order:     0,
				})
			}

		}
		//mapping data
		source, categories := parseHeader(header[0])
		reqBody := external.GraphExternalBody{
			Query: commonQuery,
			Variables: external.Variables{BigQuestion: external.BigQuestion{
				Title:             source.Value,
				Content:           "{}",
				Categories:        categories,
				SmallQuestionList: []external.SmallQuestion{smallQuestion,},
			}},
		}
		questions = append(questions, reqBody)
	}
	return questions, nil
}
